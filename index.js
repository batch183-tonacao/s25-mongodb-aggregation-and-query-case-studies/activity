// 1. Create an activity.js file on where to write and save the solution for the activity.





// 7. Create a git repository named S25.
// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 9. Add the link in Boodle.

// 2. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "onSale", totalFruitsOnSale: {$count: {}}}}
]);


// 3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
	{$match: {"stocks": {$gte: 20}}},
	{$group: {"_id": "onSale", totalFruitsMore20: {$count: {}}}}
]);


// 4. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{
       $group:
         {
           _id: "$supplier_id",
           avgPrice: { $avg: {$sum: "$price"}},
        
         }
     }
]);

// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", highestPrice: {$max: "$price" }}}
]);

// 6. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", highestPrice: {$min: "$price" }}}
]);
